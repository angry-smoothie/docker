FROM docker:dind

MAINTAINER Alexis PIRES <pires.alexis@gmail.com>

COPY dockerd-entrypoint-wrapper.sh /

RUN chmod +x /dockerd-entrypoint-wrapper.sh

ENTRYPOINT ["/dockerd-entrypoint-wrapper.sh"]